package com.example.springbootdemo.repository;

import com.example.springbootdemo.Designation;
import com.example.springbootdemo.entity.Department;
import com.example.springbootdemo.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface EmployeeRepository extends JpaRepository<Employee,Long>, JpaSpecificationExecutor<Employee>
{
    @Transactional
    @Modifying
    @Query("UPDATE Employee E SET E.department = ?2 WHERE E.id = ?1")
    void AddDepartment_id(Long employee_id, Department department);

    @Query("SELECT e FROM Employee e WHERE e.department = ?1")
    List<Employee> FindByDepartment_Id( Department department);


    @Query("SELECT e FROM Employee e WHERE e.designation = ?1")
    List<Employee> FindByDesignation( Designation designation);
}

