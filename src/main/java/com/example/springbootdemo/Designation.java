package com.example.springbootdemo;

public enum Designation {
    ASSOCIATE,
    MANAGER,
    ARCHITECT
}
