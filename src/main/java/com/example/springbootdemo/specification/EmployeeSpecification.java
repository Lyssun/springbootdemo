package com.example.springbootdemo.specification;

import com.example.springbootdemo.entity.Employee;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public class EmployeeSpecification
{

    public static Specification<Employee> getEmployeesByIdSpec(Long id) {
        return (root, query, criteriaBuilder) -> {
            return criteriaBuilder.equal(root.get("id"), id);
        };
    }

    public static Specification<Employee> getEmployeesWhoseSalaryIsOverSpec(Long min_salary) {
        return (root, query, criteriaBuilder) -> {
            query.orderBy(criteriaBuilder.asc(root.get("name")));
            return criteriaBuilder.greaterThanOrEqualTo(root.get("salary"), min_salary);
        };
    }

    public static Specification<Employee> getEmployeesWhoseSalaryIsOverAndInDeptSpec(Long min_salary, Long dept_id) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<Predicate>();
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("salary"), min_salary));
            predicates.add(criteriaBuilder.equal(root.get("department_id"), dept_id));
            query.orderBy(criteriaBuilder.desc(root.get("salary")));

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}

