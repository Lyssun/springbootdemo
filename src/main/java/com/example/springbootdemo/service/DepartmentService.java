package com.example.springbootdemo.service;

import com.example.springbootdemo.entity.Department;
import com.example.springbootdemo.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepartmentService
{
    @Autowired
    DepartmentRepository departmentRepository;

    public Iterable<Department> GetDepartments()
    {
        return departmentRepository.findAll();
    }

    public Department GetDepartmentById(Long id)
    {
        return departmentRepository.findById(id).get();
    }

    public Department CreateDepartment(Department dept)
    {
        return departmentRepository.save(dept);
    }

    public Department UpdateDepartment(Department dept)
    {
        return departmentRepository.save(dept);
    }

}
