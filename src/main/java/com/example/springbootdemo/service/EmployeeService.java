package com.example.springbootdemo.service;

import com.example.springbootdemo.Designation;
import com.example.springbootdemo.entity.Department;
import com.example.springbootdemo.entity.Employee;
import com.example.springbootdemo.repository.EmployeeRepository;
import com.example.springbootdemo.specification.EmployeeSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService
{
    @Autowired
    EmployeeRepository employeeRepository;

    public Iterable<Employee> GetEmployees()
    {
        return employeeRepository.findAll();
    }

    public List<Employee> GetEmployeesByDepartmentId(Department id)
    {
        return employeeRepository.FindByDepartment_Id(id);
    }

    public List<Employee> GetEmployeesByDesignation(String designation)
    {
        return employeeRepository.FindByDesignation(Enum.valueOf(Designation.class,designation));
    }

    public Employee GetEmployee(Long id)
    {
        return employeeRepository.findAll(EmployeeSpecification.getEmployeesByIdSpec(id)).stream().findFirst().get();
    }

    public Employee CreateEmployee(Employee emp)
    {
        return employeeRepository.save(emp);
    }

    public Employee UpdateEmployee(Employee emp)
    {
        return employeeRepository.save(emp);
    }

    public void AddDepartment(Long employee_id, Department department)
    {
        employeeRepository.AddDepartment_id(employee_id,department);
    }

    public List<Employee> GetEmployeesWithSalaryOver(Long min_salary){
        return employeeRepository.findAll(EmployeeSpecification.getEmployeesWhoseSalaryIsOverSpec(min_salary));
    }

    public List<Employee> GetEmployeesWithSalaryOverAndInDept(Long min_salary, Long dept_id){
        return employeeRepository.findAll(EmployeeSpecification.getEmployeesWhoseSalaryIsOverAndInDeptSpec(min_salary,dept_id));
    }
}
