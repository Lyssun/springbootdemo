package com.example.springbootdemo.entity;

import com.example.springbootdemo.Designation;
import com.example.springbootdemo.entitylistener.EmployeeListener;
import com.example.springbootdemo.information.EmployeeInformation;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.ManyToAny;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;

import javax.persistence.*;

@Entity
@EntityListeners(EmployeeListener.class)
@Table(name = "employee")
public class Employee {
    @Getter @Setter
    @Id
    @GeneratedValue
    @Column(name="id")
    private long id;
    @Getter @Setter
    @Column(name="name")
    private String name;
    @Getter @Setter
    @Column(name="salary")
    private Long salary;
    @Getter @Setter
    @ManyToOne
    @JoinColumn(name="department_id")
    private Department department;
    @Getter @Setter
    @ManyToOne
    @JoinColumn(name="manager_id")
    private Employee manager;
    @Getter @Setter
    @NonNull
    @Enumerated(EnumType.STRING)
    @Column(name="designation")
    private Designation designation;
    @Embedded
    @Getter @Setter
    private EmployeeInformation information;

    public Employee() {
    }

    public Employee(String name, Long salary, Department department, Employee manager, Designation designation, EmployeeInformation info)
    {
        this.name = name;
        this.salary = salary;
        this.department = department;
        this.manager = manager;
        this.designation = designation;
        this.information = info;
    }
}
