package com.example.springbootdemo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Department {

    @Getter @Setter
    private @Id @GeneratedValue long id;
    @Getter @Setter
    private String name;

    public Department() {
    }

    public Department(String name)
    {
        this.name = name;
    }

    //region Getter
    public long getId() {
        return id;
    }
    public String getName() {
        return this.name;
    }
    //endregion
    //region Setter
    private void setID(long id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    //endregion

    @Override
    public String toString() {
        return "Employee{" + "id=" + this.id + ", name='" + this.name + '\'' + '}';
    }
}
