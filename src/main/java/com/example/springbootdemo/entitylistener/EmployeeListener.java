package com.example.springbootdemo.entitylistener;

import com.example.springbootdemo.entity.Employee;

import javax.persistence.*;

public class EmployeeListener {
    @PrePersist
    public void userPrePersist(Employee ob) {
        System.out.println("Listening Employee Pre Persist : " + ob.getId());
    }
    @PostPersist
    public void userPostPersist(Employee ob) {
        System.out.println("Listening Employee Post Persist : " + ob.getId());
    }
    @PostLoad
    public void userPostLoad(Employee ob) {
        System.out.println("Listening Employee Post Load : " + ob.getId());
    }
    @PreUpdate
    public void userPreUpdate(Employee ob) {
        System.out.println("Listening Employee Pre Update : " + ob.getId());
    }
    @PostUpdate
    public void userPostUpdate(Employee ob) {
        System.out.println("Listening Employee Post Update : " + ob.getId());
    }
    @PreRemove
    public void userPreRemove(Employee ob) {
        System.out.println("Listening Employee Pre Remove : " + ob.getId());
    }
    @PostRemove
    public void userPostRemove(Employee ob) {
        System.out.println("Listening Employee Post Remove : " + ob.getId());
    }
}
