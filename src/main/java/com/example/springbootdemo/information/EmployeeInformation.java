package com.example.springbootdemo.information;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;

@Embeddable
public class EmployeeInformation {
    @Getter @Setter
    private String email;
    @Getter @Setter
    private String phoneNumber;

    public EmployeeInformation(String email, String phoneNumber) {
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public EmployeeInformation() {

    }
}
