package com.example.springbootdemo.controller;

import com.example.springbootdemo.Designation;
import com.example.springbootdemo.entity.Employee;
import com.example.springbootdemo.information.EmployeeInformation;
import com.example.springbootdemo.service.DepartmentService;
import com.example.springbootdemo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService service;

    @Autowired
    private DepartmentService deptService;


    @RequestMapping(value="/findAll")
    public Iterable<Employee> GetEmployees() {
        return service.GetEmployees();
    }

    @RequestMapping(value="/find")
    public Employee GetEmployeeById(@RequestParam Long employee_id) {
        return service.GetEmployee(employee_id);
    }

    @RequestMapping(value="/findByDept")
    public Iterable<Employee> GetEmployeeByDepartmentId(@RequestParam Long department_id) {
        return service.GetEmployeesByDepartmentId(deptService.GetDepartmentById((department_id)));
    }

    @RequestMapping(value="/new")
    public Employee Create(String name, Long salary, Long department, Long manager,@RequestParam String designation, String email, String phoneNumber) {
        return service.CreateEmployee(new Employee( name, salary, department != null ? deptService.GetDepartmentById(department) : null , manager != null ? service.GetEmployee(manager) : null, Enum.valueOf(Designation.class,designation), new EmployeeInformation(email, phoneNumber)));
    }

    @RequestMapping(value="/update")
    public Employee update(@RequestParam Long employee_id, String name, Long salary, Long department, String designation) {
        Employee emp = service.GetEmployee(employee_id);
        if(emp != null)
        {
            if(name != null) emp.setName(name);
            if(salary != null) emp.setSalary(salary);
            if(department != null) emp.setDepartment(deptService.GetDepartmentById(department));
            if(designation != null) emp.setDesignation(Enum.valueOf(Designation.class,designation));

        }
        return service.UpdateEmployee(emp);
    }

    @RequestMapping(value="/add_department")
    public void addDepartment(@RequestParam Long employee_id, Long department_id) {
        service.AddDepartment(employee_id, deptService.GetDepartmentById((department_id)));
    }

    @RequestMapping(value="/SalaryOver10k")
    public Iterable<Employee> SalaryOver10k() {
        return service.GetEmployeesWithSalaryOver(Long.valueOf(100000));
    }

    @RequestMapping(value="/SalaryOver10kAndInDept")
    public Iterable<Employee> SalaryOver10kAndInDept(@RequestParam Long department_id) {
        return service.GetEmployeesWithSalaryOverAndInDept(Long.valueOf(100000), department_id);
    }

    @RequestMapping(value="/findByDesignation")
    public Iterable<Employee> GetEmployeesByDesignation(@RequestParam String designation) {
        return service.GetEmployeesByDesignation(designation);
    }


}