package com.example.springbootdemo.controller;

import com.example.springbootdemo.entity.Department;
import com.example.springbootdemo.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/department")
public class DepartmentController {

    @Autowired
    private DepartmentService service;

    @RequestMapping(value="/findAll")
    public Iterable<Department> GetDepartments() {
        return service.GetDepartments();
    }

    @RequestMapping(value="/find")
    public Department GetDepartmentById(@RequestParam Long department_id) {
        return service.GetDepartmentById(department_id);
    }

    @RequestMapping(value="/new")
    public @ResponseBody Department Create( String name) {
        return service.CreateDepartment(new Department(name));
    }

    @RequestMapping(value="/update")
    public @ResponseBody Department update(@RequestParam Long department_id, String name) {
        Department department = service.GetDepartmentById(department_id);
        if(department != null)
        {
            if(name != null) department.setName(name);
        }
        return service.UpdateDepartment(department);
    }

}